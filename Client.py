############### Client Bot ##################
#!/usr/bin/python3
import discord
import datetime
import json
import time
import logging
from dotenv import load_dotenv
import requests
import randfacts

logging.basicConfig(level=logging.INFO)
'''
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)


@tasks.loop(seconds=2, count=10)
async def spamer(ctx):
    x = randfacts.getFact()
    await ctx.send(x)

@bot.command()
async def startspamer(ctx):
    spamer.start(ctx)

@bot.command()
async def stopspamer():
    spamer.stop()
    
@tasks.loop(minutes=1)
async def testM():
    print("Test")
    x = "Tesadfasdf"
    #user = bot.get_user(234396214555049994)
    user = client.get_user(234396214555049994)
    await user.send('👀')
#testM.start()

user = await bot.get_user_info(int(abonnent[3]))
await bot.send_message(user, x)
print("Privatenachricht", x)
updateTime(id, 24)
'''

@client.command()
async def help(ctx):
    embed = discord.Embed(
        title='Hilfe',
        colour = discord.Colour.green()
    )
    embed.set_thumbnail(url='profil.jpg')

    embed.add_field(name='!help', value='Ruft die Hilfe auf, du Dussel', inline=False)
    embed.add_field(name='!link', value='Link zum Stundenplan', inline=False)
    embed.add_field(name='!stundenplan', value='Gesamten Stundenplan bzw. mit Argumente 1=Montag, 7=Sonntag. !stundenplan 1 = Stundenplan für Montag', inline=False)
    embed.add_field(name='!stundenplanabo', value='Stellt Benachrichtigungen ein, Standard Uhrzeit 7:40 UTC!, !stundenplanabo 08:30 für Abo um 9:30 GMT+1', inline=False)
    embed.add_field(name='!heute', value='Stundenplan von heute', inline=False)
    embed.add_field(name='!morgen', value='Stundenplan von morgen', inline=False)
    embed.add_field(name='!klausuren', value='Alle Klausuren', inline=False)
    embed.add_field(name='!nächsteklausur', value='Gibt die nächste Klausur wieder + Tage', inline=False)
    embed.add_field(name='!ping', value='Wiederhole was du gesagt hast', inline=False)
    embed.add_field(name='!sprichwort', value='Lustige Sprichwörter, !sprichwort Marco = Sprichwort von Marco. Achtung Name groß.', inline=False)
    embed.add_field(name='!sprichwortliste', value='Alle Sprichwörter, bzw. !sprichwortliste Marco = alle Sprichwörter von Marco. Achtung Name groß', inline=False)
    embed.add_field(name='!fakt', value='Ein Fakt? Kommt sofort!', inline=False)
    embed.add_field(name='!startfakten', value='DU WILLST FAKTEN? Du bekommst gleich 10!', inline=False)
    embed.add_field(name='!stopfakten', value='ok, das sind zu viele Fakten', inline=False)

    embed.set_footer(text="Beep Boop. I\'m a bot")
    await ctx.send(embed=embed)




authClient = ''
with open('auth.txt') as f:
    authClient = f.readline()

client = discord.Client()
current_time = datetime.datetime.now()
today = datetime.date.today()
reminder = True

@client.event
async def on_ready():
    print("Client läuft")
    channel = client.get_channel(785556897783742509)
    #await channel.send('Eingeloggt als {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if 'hilfe' in message.content:
        await message.channel.send("schon probiert neuzustarten?")

    if message.content.startswith('Test'):
        await message.channel.send("Funktioniert auf dem Server hier :)")

    if 'tundenplan' in message.content:
        with open('stundenplan') as json_file:
            data = json.load(json_file)
            for p in data['Modules']:
                await message.channel.send(p['title'])

    if message.content.startswith('heute'):
        await message.channel.send("Heute sind folgende Vorstellungen: ")
        with open('stundenplan') as json_file:
            data = json.load(json_file)
            for p in data['Modules']:
                if p['day'] == today.strftime('%A'):
                    await message.channel.send(p['title'] + ' - ' + p['type'] + '\nStart: \t' + p['start'] + '\nEnde: \t' + p['end'] + '\nLink: ' + p['link'])

    if message.content.startswith('morgen'):
        await message.channel.send("Morgen sind folgende Vorstellungen: ")
        with open('stundenplan') as json_file:
            data = json.load(json_file)
            for p in data['Modules']:
                if p['day'] == (today + datetime.timedelta(days=1)).strftime('%A'):
                    await message.channel.send(p['title'] + ' - ' + p['type'] + '\nStart: \t' + p['start'] + '\nEnde: \t' + p['end'] + '\nLink: ' + p['link'])





client.run(authClient)