import ask_sdk_core.utils as ask_utils
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_model import Response

############# Alexa ####################
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def getResponseFromAPI():
    url = 'https://api.chucknorris.io/jokes/random'
    response = requests.get(url)
    print(response.status_code)

    json_data = response.json()
    joke = json_data['value']
    return joke


'''
class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Welcome, you can say Hello or Help. " \
                       "Which would you like to try?"

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )
'''


class JokeIntentHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        return ask_utils.is_intent_name("JokeIntent")(handler_input)

    def handle(self, handler_input):
        speak_output = "Here's a sample joke for you."

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


joker = JokeIntentHandler()
print(joker.handle())