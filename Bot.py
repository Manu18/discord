#!/usr/bin/python3
import discord
import json
import time
import logging
from dotenv import load_dotenv
from discord.ext import commands, tasks
import randfacts
from datetime import datetime, timedelta, date, time
from google.cloud import firestore
import os
import random
import hashlib
from PIL import Image, ImageFont, ImageDraw
from io import BytesIO

################## Basic Inits ################
fileHandle = open('auth.txt', "r")
lines = fileHandle.readlines()
fileHandle.close()
authBot = lines[0].rstrip("\n")
project_id = 'able-reef-297917'
credential_path = 'firestoreAuth.json'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path

########## DB #####################

db = firestore.Client()

'''
doc_ref = db.collection(u'stundenplan').document(u'Betriebssysteme Ü')
doc_ref.set({
    "fach": "Betriebssysteme",
    "typ": "Uebung",
    "tag": "Montag",
    "start": "09:00",
    "end": "10:30",
    "prof": "Ewert",
    "link": "https://h-brs.webex.com/webappng/sites/h-brs/meeting/download/ce27d19377154083857236b33c711547?MTID=m3379b901dcd94a1e7f569a15d482148d&siteurl=h-brs",
    'semester': 3
})
channel = bot.get_channel(701735405002817539)
'''

############ Discord Bot ##############
logging.basicConfig(level=logging.INFO)
'''
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
'''

####################### Bot ###############################
load_dotenv()
bot = commands.Bot(command_prefix='!')
bot.remove_command("help")


# help
@bot.command()
async def help(ctx):
    embed = discord.Embed(
        title='Hilfe',
        colour=discord.Colour.green()
    )
    embed.set_thumbnail(url='http://uni.manuelfischer.eu/profil.jpg')

    # embed.add_field(name='!help', value='Ruft die Hilfe auf, du Dussel', inline=False)
    embed.add_field(name='!leaderboard [anzahl]',
                    value='Aktuelle Ränge der Top 5, bzw !leaderboard 10 gibt die Top 10 zurück', inline=False)
    embed.add_field(name='!rang [@user]', value='Gibt deinen Rang zurück oder eines Users mit !rang @Manuel F.',
                    inline=False)
    embed.add_field(name='!wanted [@user]', value='Gibt ein Wanted Bild zurück oder eines Users mit !wanted @Manuel F.',
                    inline=False)
    embed.add_field(name='!link', value='Link zum Stundenplan', inline=False)
    embed.add_field(name='!stundenplan [tagesnummer] ["spezialisierung"] ["wahlpflicht"]',
                    value='Gesamten Stundenplan bzw. mit Argumente 1=Montag, 7=Sonntag. !stundenplan 1 = Stundenplan für Montag.'
                          'Als Spezialisierung bsp. "Informationssicherheit", Wahlfpflicht "Hackerpraktikum"'
                          'Für benutzerspezifische Stundenpläne !neuerstundenplan aufrufen',
                    inline=False)
    embed.add_field(name='!neuerstundenplan ["spezialisierung"] ["spezialisierungsFächer] ["wahlpflicht"] ["gruppen"]',
                    value='Bsp: !neuerstundenplan "Informationssicherheit" "Mobile Sicherheit, Sicherheit von Web-Anwendungen" "Hackerpraktikum"',
                    inline=False)
    embed.add_field(name='!stundenplanabo [uhrzeit]',
                    value='Stellt Benachrichtigungen ein, Standard Uhrzeit 7:40 UTC!, !stundenplanabo 08:30 für Abo um 9:30 GMT+1',
                    inline=False)
    embed.add_field(name='!heute', value='Stundenplan von heute', inline=False)
    embed.add_field(name='!morgen', value='Stundenplan von morgen', inline=False)
    embed.add_field(name='!klausuren', value='Alle Klausuren', inline=False)
    embed.add_field(name='!nächsteklausur', value='Gibt die nächste Klausur wieder + Tage', inline=False)
    embed.add_field(name='!klausurenabo [uhrzeit]',
                    value='Stellt Benachrichtigungen ein, Standard Uhrzeit 7:40 UTC!, !klausurenabo 08:30 für Abo um 9:30 GMT+1',
                    inline=False)
    embed.add_field(name='!ping [text]', value='Wiederhole was du gesagt hast', inline=False)
    embed.add_field(name='!sprichwort [name]',
                    value='Lustige Sprichwörter, !sprichwort Marco = Sprichwort von Marco. Achtung Name groß.',
                    inline=False)
    embed.add_field(name='!sprichwortliste [name]',
                    value='Alle Sprichwörter, bzw. !sprichwortliste Marco = alle Sprichwörter von Marco. Achtung Name groß',
                    inline=False)
    embed.add_field(name='!neuessprichwort [name] "[sprichwort]"',
                    value='Einfügen eines neuen Sprichwortes mit !sprichwort Autor "Hier das Sprichwort"',
                    inline=False)
    embed.add_field(name='!fakt', value='Ein Fakt? Kommt sofort!', inline=False)
    embed.add_field(name='!startfakten', value='Interessante Fakten gewünscht? Du bekommst gleich 10!', inline=False)
    embed.add_field(name='!stopfakten', value='ok, das sind zu viele Fakten', inline=False)
    embed.add_field(name='!geburtstage', value='Liste unserer Geburtstage', inline=False)
    # embed.set_footer(text="Beep Boop. I\'m a bot")
    await ctx.send(embed=embed)
    await ctx.message.delete()


@bot.command()
async def hilfe(ctx):
    await help(ctx)
    await ctx.message.delete()


@bot.command()
@commands.has_role("Moderator")
async def clear(ctx, amount=1):
    if amount < 20:
        await ctx.channel.purge(limit=amount)
    await ctx.message.delete()


@bot.event
async def on_ready():
    print("Bot läuft")
    channelAllgemein = bot.get_channel(785556897783742509)
    # await channelAllgemein.send(f'{bot.user.name} hat sich zu euch gesellt!')


@bot.command()
async def ping(ctx, arg):
    await ctx.send(arg)
    await ctx.message.delete()


@bot.command()
async def fakt(ctx):
    x = randfacts.getFact()
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def startfakten(ctx):
    fakter.start(ctx)
    await ctx.message.delete()


@bot.command()
async def stopfakten(ctx):
    fakter.stop()
    await ctx.message.delete()


@tasks.loop(seconds=5, count=10)
async def fakter(ctx):
    x = randfacts.getFact()
    await ctx.send(x)


@bot.command()
async def link(ctx):
    x = "Vielleicht hilft Dir <https://uni.manuelfischer.eu/> weiter"
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def klausuren(ctx):
    x = "**Klausuren**:\n"
    kl = db.collection(u'klausuren')
    docs = kl.stream()
    listKL = []
    for doc in docs:
        name = str(f'{doc.id}')
        p = doc.to_dict()
        listKL.append([name, p['datum'], p['uhrzeit'], p['hilfsmittel']])
    sorted_listKL = sorted(listKL, key=lambda tag: tag[1], reverse=False)
    for dict in sorted_listKL:
        x += '**' + dict[0] + ':**\n'
        x += 'Datum: ' + str(dict[1]) + ' ' + str(dict[2]) + '\n'
        x += 'Hilfsmittel: ' + str(dict[3])
        x += '\n \n'
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def klausur(ctx):
    await klausuren(ctx)


@bot.command()
async def nächsteklausur(ctx):
    x = nextKlausur()
    await ctx.send(x)
    await ctx.message.delete()


def updateabo(typ):
    kl = db.collection(typ)
    docs = kl.stream()
    cache = []
    for doc in docs:
        dict = doc.to_dict()
        tupel = [str(dict['channel']), str(dict['uhrzeit']), str(dict['semester']), str(dict['user'])]
        cache.append(tupel)
    return cache


def updateTime(channelID, intervall=24, typ='klausurenabonnenten'):
    kl = db.collection(typ)
    docs = kl.stream()
    tag = datetime.today().weekday()
    if tag == 4:
        hours = timedelta(hours=intervall + 48)
    else:
        hours = timedelta(hours=intervall)
    for doc in docs:
        dict = doc.to_dict()
        if str(dict['channel']) == str(channelID):
            #print("Channel gefunden, Zeit wird ersetzt")
            current_time = datetime.strptime(dict['uhrzeit'], "%d.%m.%Y %H:%M")
            futureD = current_time + hours
            data = db.collection(typ).document(str(channelID))
            futureDS = str(futureD.strftime("%d.%m.%Y %H:%M"))
            data.update({u'uhrzeit': futureDS})
            print("Zeitupdate", futureDS)


@bot.command()
async def klausurenabo(ctx, zeit='07:45', semester=3):
    channels = updateabo('klausurenabonnenten')
    # print("Aboliste: ", channels)
    list = [item[0] for item in channels]
    id = str(ctx.channel.id)
    userID = str(ctx.message.author.id)
    neu = True
    for el in list:
        if el == id:
            neu = False
    if neu or not channels:
        doc_ref = db.collection(u'klausurenabonnenten').document(id)
        dayNow = datetime.today().strftime('%d.%m.%Y')
        doc_ref.set({
            'uhrzeit': str(dayNow) + ' ' + str(zeit),
            u'channel': id,
            'semester': semester,
            'user': userID
        })
        x = "Der Channel wird jeden Tag über Klausur benachrichtigt."
        await ctx.send(x)
        await ctx.message.delete()
    else:
        x = "Bereits eingetragen"
        await ctx.send(x)
        await ctx.message.delete()


def nextKlausur():
    kl = db.collection(u'klausuren')
    docs = kl.stream()
    klausuren = []
    for doc in docs:
        dict = doc.to_dict()
        tupel = [str(dict['fach']), str(dict['datum'])]
        klausuren.append(tupel)
    nk = klausuren[0]
    nkD = datetime.strptime(nk[1], "%d.%m.%Y")
    for k in range(len(klausuren)):
        kk = datetime.strptime(klausuren[k][1], "%d.%m.%Y")
        if nkD > kk:
            nk = klausuren[k]
            nkD = kk
    diff = (nkD - datetime.now()).days
    x = 'In **' + str(diff) + ' Tagen** ist die Klausur von **' + str(nk[0]) + '**.'
    return x


@tasks.loop(minutes=30)
async def klausurbenachrichtigung():
    print("Klausurenloop läuft")
    channels = updateabo('klausurenabonnenten')
    if channels:
        for abonnent in channels:
            try:
                delta = datetime.strptime(abonnent[1], "%d.%m.%Y %H:%M") - datetime.now()
                if delta < timedelta(minutes=30):
                    id = int(abonnent[0])
                    chan = bot.get_channel(id)
                    x = nextKlausur()
                    try:
                        await chan.send(x)
                        print("Klausurbenachrichtigung", x)
                        typ = 'klausurenabonnenten'
                        updateTime(id, 24, typ)
                    except:
                        print("Kein Channel")
            except:
                print("Falsches Zeitformat oder anderer Fehler")


#klausurbenachrichtigung.start()


def generierStundenplan(spezialiserung, wahlpflicht, spezFaecher, gruppen):
    x = ''
    for i in range(5):
        if i == 1:
            x += '<DI>'
        if i == 3:
            x += '<DO>'
        x += str(generierstundenplanTag(i, spezialiserung, wahlpflicht, spezFaecher, gruppen)) + '\n'
    return x


def generierstundenplanTag(tag, spezialiserung, wahlpflicht, spezFaecher, gruppen):
    #print(spezialiserung, wahlpflicht, spezFaecher, gruppen)
    tage = ['Mo', 'Di', 'Mi', 'Do', 'Fr']
    tageV = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag']
    x = '**' + str(tageV[tag]) + '**:\n'
    st = db.collection('StundenplanWS2021')
    docs = st.stream()
    listSP = []
    for doc in docs:
        p = doc.to_dict()
        #and p['gruppe'] in gruppen p['spezialisierung'] in spezialiserung p['spezialisierung'] == "Wahlpflicht" and
        if tage[tag] == p['tag'] and (((p['fach'] in spezFaecher) or (p['fach'] in wahlpflicht)) or \
            (not spezFaecher and (p['spezialisierung'] in spezialiserung or p['spezialisierung'] == "Alle"))):
            listSP.append([p['fach'], p['typ'], p['spezialisierung'], p['start'], p['end'], p['link'], p['gruppe'], p['prof'], p['raum']])
    sorted_listSP = sorted(listSP, key=lambda tag: tag[3], reverse=False)
    for l in sorted_listSP:
        gS = ''
        if l[6]:
            gS = ' - (Gr. ' + str(l[6]) + ')'
        if l[2] != "":
            x += l[0] + ' - ' + l[1] + ' (' + l[2] + ') - ' + str(l[7]) + gS + '\nVon ' + str(l[3]) + \
                 ' bis ' + str(l[4]) + '\nRaum: ' + l[8] + '\nLink: <' + l[5] + '>\n\n'
        else:
            x += l[0] + ' - ' + l[1] + ' - ' + str(l[7]) + gS + '\nVon ' + str(l[3]) + \
                 ' bis ' + str(l[4]) + '\nRaum: ' + l[8] + '\nLink: <' + l[5] + '>\n\n'
    return x


standardSpezialisierung = "Informationssicherheit, Visual Computing, Alle, " \
                          "Cyber-Physical Systems, Bioinformatik, Wahlpflicht"
standardGruppen = ""
standardWahlpflicht = ""
standardSpezFaecher = ""
standardFaecher = "Einführung IT-Recht, Software Engineering 2, Literatur-Seminar"


def getPersDaten(userid):
    userDB = db.collection('Nutzer')
    docs = userDB.stream()
    for doc in docs:
        p = doc.to_dict()
        if userid == p['id']:
            return (p['spezialisierung'], p['wahlpflicht'], p['spezFaecher'], p['gruppen'])
    return (standardSpezialisierung, standardWahlpflicht, standardSpezFaecher, standardGruppen)


@bot.command()
async def neuerstundenplan(ctx, spezialisierung, spezFaecher="", wahlpflicht="", gruppen=""):
    idU = ctx.message.author.id
    userDB = db.collection(u'Nutzer').document(str(idU))
    daten = {
        "spezialisierung": spezialisierung,
        "wahlpflicht": wahlpflicht,
        "gruppen": gruppen,
        "spezFaecher": spezFaecher + ", " + standardFaecher,
        "id": idU
    }
    doc = userDB.get()
    if doc.exists:
        userDB.update(daten)
    else:
        userDB.set(daten)
    await ctx.send("Daten gespeichert")


@bot.command()
async def stundenplan(ctx, arg=-1, spezialiserung=standardSpezialisierung, wahlpflicht=standardWahlpflicht,
                      spezFaecher=standardSpezFaecher, gruppen=standardGruppen):
    x = ''
    if spezialiserung == standardSpezialisierung:
        persDaten = getPersDaten(ctx.message.author.id)
        spezialiserung = persDaten[0].split(', ')
        wahlpflicht = persDaten[1].split(', ')
        spezFaecher = persDaten[2].split(', ')
        gruppen = persDaten[3].split(', ')
    else:
        spezialiserung = spezialiserung.split(', ')
        wahlpflicht = wahlpflicht.split(', ')
        spezFaecher = spezFaecher.split(', ')
        gruppen = gruppen.split(', ')

    if arg == -1:
        x = "Stundenplan: \n" + str(generierStundenplan(spezialiserung, wahlpflicht, spezFaecher, gruppen))
    else:
        x = "Stundenplan: \n" + str(generierstundenplanTag((int(arg) - 1) % 7, spezialiserung, wahlpflicht, spezFaecher, gruppen))
    if '<DI>' in x or '<DO>' in x:
        lx = x.split('<DI>')
        await ctx.send(lx[0])
        lx2 = lx[1].split('<DO>')
        await ctx.send(lx2[0])
        await ctx.send(lx2[1])
    else:
        await ctx.send(x)


@bot.command()
async def heute(ctx):
    tag = datetime.today().weekday()
    persDaten = getPersDaten(ctx.message.author.id)
    x = str(generierstundenplanTag(tag, spezialiserung=persDaten[0].split(', '), wahlpflicht=persDaten[1].split(', '),
                               spezFaecher=persDaten[2].split(', '), gruppen=persDaten[3].split(', ')))
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def morgen(ctx):
    tag = (datetime.today().weekday() + 1) % 7
    persDaten = getPersDaten(ctx.message.author.id)
    x = str(generierstundenplanTag(tag, spezialiserung=persDaten[0].split(', '), wahlpflicht=persDaten[1].split(', '),
                               spezFaecher=persDaten[2].split(', '), gruppen=persDaten[3].split(', ')))
    await ctx.send(x)
    await ctx.message.delete()


def getSprichwort(autor=None):
    sp = db.collection(u'sprichwörter')
    docs = sp.stream()
    x = ''
    if autor is not None:
        docs = sp.where(u'autor', u'==', autor).stream()
    listSp = []
    for doc in docs:
        p = doc.to_dict()
        listSp.append(p)
        # if autor == p['autor'] or autor is None:
    spDic = random.choice(listSp)
    x = str(spDic['autor']) + ': "' + str(spDic['sprichwort']) + '"'
    return x


def setSprichwort(autor, wort):
    doc_ref = db.collection(u'sprichwörter').document()
    doc_ref.set({
        "autor": str(autor),
        "sprichwort": str(wort)
    })
    print("Neues Sprichwort: ", wort)
    return "Neues Sprichwort gespeichert"


def allSprichwort(autor):
    sp = db.collection(u'sprichwörter')
    docs = sp.stream()
    x = '**Liste aller Sprichwörter**: \n'
    if autor is not None:
        docs = sp.where(u'autor', u'==', autor).stream()
    for doc in docs:
        spDic = doc.to_dict()
        x += str(spDic['autor']) + ': "' + str(spDic['sprichwort']) + '"\n'
    return x


@bot.command()
async def sprichwort(ctx, autor=None):
    x = getSprichwort(autor)
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def sprichwortliste(ctx, autor=None):
    x = allSprichwort(autor)
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def neuessprichwort(ctx, autor, wort):
    x = setSprichwort(autor, wort)
    await ctx.send(x)


@bot.command()
async def stundenplanabo(ctx, zeit='07:45', semester=4):
    channels = updateabo('stundenplanabonnenten')
    list = [item[0] for item in channels]
    id = str(ctx.channel.id)
    userID = str(ctx.message.author.id)
    neu = True
    for el in list:
        if el == id:
            neu = False
    if neu or not channels:
        doc_ref = db.collection(u'stundenplanabonnenten').document(id)
        dayNow = datetime.today().strftime('%d.%m.%Y')
        doc_ref.set({
            'uhrzeit': str(dayNow) + ' ' + str(zeit),
            u'channel': id,
            'semester': semester,
            'user': userID
        })
        x = "Der Channel wird jeden Tag über den Stundenplan benachrichtigt."
        await ctx.send(x)
        await ctx.message.delete()
    else:
        x = "Bereits eingetragen"
        await ctx.send(x)
        await ctx.message.delete()


@tasks.loop(minutes=15)
async def stundenplanbenachrichtigung():
    print("Stundenplanloop läuft")
    channels = updateabo('stundenplanabonnenten')
    if channels:
        for abonnent in channels:
            try:
                delta = datetime.strptime(abonnent[1], "%d.%m.%Y %H:%M")
                delta = delta - datetime.now()
                if delta < timedelta(minutes=15):
                    id = int(abonnent[0])

                    chan = bot.get_channel(id)
                    #chan = None
                    #for ch in bot.private_channels:
                    #    if ch.id == id:
                    #        chan = ch

                    tag = datetime.today().weekday()
                    persDaten = getPersDaten(int(abonnent[3]))
                    spezialiserung = persDaten[0].split(', ')
                    wahlpflicht = persDaten[1].split(', ')
                    spezFaecher = persDaten[2].split(', ')
                    gruppen = persDaten[3].split(', ')

                    x = generierstundenplanTag(tag, spezialiserung, wahlpflicht, spezFaecher, gruppen)
                    try:
                        await chan.send(x)
                        #print("Stundenplanbenachrichtigung", x)
                        typ = 'stundenplanabonnenten'
                        updateTime(id, 24, typ)
                    except:
                        print("Kein Channel")
            except:
                print("Falsches Zeitformat oder anderer Fehler")


stundenplanbenachrichtigung.start()


def getGeburtstag(date=None):
    gb = db.collection('geburtstage')
    docs = gb.stream()
    x = ''
    if date is not None:
        docs = gb.where('geburtstag', '>=', date.strftime("%d.%m")).where('geburtstag', '<',
                                                                          date.strftime("%d.%m") + '9').stream()
    listGB = []
    for doc in docs:
        p = doc.to_dict()
        name = p['name']
        tag = datetime.strptime(p['geburtstag'][:-5], "%d.%m")
        id = p['id']
        listGB.append([name, tag, id])
    sorted_listGB = sorted(listGB, key=lambda tag: tag[1], reverse=False)
    if date is None:
        for gbDic in sorted_listGB:
            x += '**' + str(gbDic[0]) + '** hat Geburtstag am ' + str(gbDic[1].strftime("%d.%m")) + '\n'
    else:
        userID = '<@' + str(listGB[0][2]) + '>'
        x = '**' + str(listGB[0][0]) + '** hat heute Geburtstag.\nAlles Gute zum Geburtstag ' + str(userID)
    return x


@bot.command()
async def geburtstage(ctx):
    x = getGeburtstag()
    await ctx.send(x)
    await ctx.message.delete()


@bot.command()
async def geburtstag(ctx):
    await geburtstage(ctx)


def is_time_between(begin_time, end_time, check_time=None):
    check_time = check_time or datetime.now().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else:  # crosses midnight
        return check_time >= begin_time or check_time <= end_time


@tasks.loop(hours=1)
async def geburtstagsbenachrichtigung():
    print("Geburtstagsloop läuft")
    channels = [701735405002817540]
    todayGB = datetime.today().date()
    try:
        geburtstag = getGeburtstag(todayGB)
    except:
        print("Kein GB oder Fehler bei getGB()")
    a = time(8, 0)
    e = time(9, 0)
    if is_time_between(a, e):
        for ch in channels:
            try:
                chan = bot.get_channel(ch)
                await chan.send(geburtstag)
                print("Geburtstagsbenachrichtigung")
            except:
                print("Channel in GB nicht gefunden")


geburtstagsbenachrichtigung.start()


########### Ranking ############
def calcXP(text):
    if text.startswith('!') or len(text) <= 3:
        return 0
    base = 3
    lenS = 0
    uniq = 4
    l = len(text)
    if l > 20:
        lenS = 1
    elif l > 40:
        lenS = 2
    elif l > 80:
        lenS = 3
    elif l > 150:
        lenS = 4
    elif l > 300:
        lenS = 5
    if not unique(text):
        return 2
    if len(text) > 3:
        saveHash(text)
    xp = base + lenS + uniq
    return xp


def unique(text):
    lines = []
    s = text.lower()
    hash = int(hashlib.sha1(s.encode("utf-8")).hexdigest(), 16) % (10 ** 8)
    with open('messages.txt') as f:
        lines = [line.rstrip('\n') for line in f]
    for l in lines:
        if hash == l:
            return False
    return True


def saveHash(text):
    s = text.lower()
    hash = int(hashlib.sha1(s.encode("utf-8")).hexdigest(), 16) % (10 ** 8)
    with open("messages.txt", mode='a') as file:
        file.write(str(hash) + '\n')


async def update_data(users, user):
    if not f'{user}' in users:
        users[f'{user}'] = {}
        users[f'{user}']['experience'] = 0
        users[f'{user}']['level'] = 1
        users[f'{user}']['messages'] = 0
        users[f'{user}']['time'] = datetime.now().strftime("%d-%b-%Y %H:%M:%S.%f")
        users[f'{user}']['user'] = f'{user}'


async def add_experience(users, user, exp):
    usertime = datetime.strptime(users[f'{user}']['time'], "%d-%b-%Y %H:%M:%S.%f")
    delta = datetime.now() - usertime
    if delta > timedelta(seconds=10):
        users[f'{user}']['experience'] += exp
        users[f'{user}']['time'] = datetime.now().strftime("%d-%b-%Y %H:%M:%S.%f")
        users[f'{user}']['messages'] += 1
    else:
        if exp > 0:
            users[f'{user}']['experience'] += 1  # xp für schnelle nachricht
            users[f'{user}']['time'] = datetime.now().strftime("%d-%b-%Y %H:%M:%S.%f")
            users[f'{user}']['messages'] += 1
    # print("XP hinzugefügt")


async def level_up(users, user, message):
    with open('users.json', 'r') as g:
        levels = json.load(g)
    experience = users[f'{user}']['experience']
    lvl_start = users[f'{user}']['level']
    lvl_end = int(experience ** (1 / 4))
    if lvl_start < lvl_end:
        channel = bot.get_channel(783976345591218187)
        # channel2 = bot.get_channel(785556897783742509)
        await channel.send(f'{user.mention} ist zu Level {lvl_end} aufgestiegen')
        # await channel2.send(f'{user.mention} ist zu Level {lvl_end} aufgestiegen')
        users[f'{user}']['level'] = lvl_end
    # print("User ist Level aufgestiegen")


@tasks.loop(hours=12)
async def xpToDB():
    with open('users.json', 'r') as json_file:
        data = json.load(json_file)
    for key, value in data.items():
        lvl = value['level']
        xp = value['experience']
        messages = value['messages']
        user = value['user']
        time = value['time']
        doc_ref = db.collection(u'ranking').document(user)
        doc_ref.set({
            "user": user,
            "xp": xp,
            "level": lvl,
            "messages": messages,
            "time": time
        })
    print("XP in DB geschrieben")


xpToDB.start()


@bot.command()
async def leaderboard(ctx, arg=5):
    embed = discord.Embed(
        title='Leaderboard',
        colour=discord.Colour.green()
    )
    userlist = []
    with open('users.json', 'r') as json_file:
        data = json.load(json_file)
    for key, value in data.items():
        lvl = value['level']
        xp = value['experience']
        messages = value['messages']
        user = value['user']
        userlist.append([user, lvl, xp, messages])
    counter = 1
    sorted_userlist = sorted(userlist, key=lambda student: student[2], reverse=True)
    for e in sorted_userlist:
        username = e[0].split("#")[0] + ' (XP: ' + str(e[2]) + ') Level: ' + str(e[1])
        embed.add_field(name="Rang " + str(counter), value=username, inline=False)
        counter += 1
        if counter > arg:
            await ctx.send(embed=embed)
            return
    await ctx.send(embed=embed)
    await ctx.message.delete()


@bot.command()
async def rang(ctx, arg=None):
    userA = ctx.message.author
    if arg:
        aS = arg.split('!')[1]
        userA = bot.get_user(int(aS[:-1]))
        test = "243adsf"
    embed = discord.Embed(
        title='Dein Rang',
        colour=discord.Colour.green()
    )
    userlist = []
    with open('users.json', 'r') as json_file:
        data = json.load(json_file)
    for key, value in data.items():
        lvl = value['level']
        xp = value['experience']
        messages = value['messages']
        user = value['user']
        userlist.append([user, lvl, xp, messages])
    counter = 1
    sorted_userlist = sorted(userlist, key=lambda student: student[2], reverse=True)
    sended = False
    for e in sorted_userlist:
        if str(e[0].split('#')[0]) == str(userA.name):
            userRang = e[0].split("#")[0] + ' du hast:\n' \
                                            'XP: ' + str(e[2]) + '\nLevel: ' + str(
                e[1]) + '\nGeschriebene Nachrichten: ' + str(e[3])
            embed.set_thumbnail(url=userA.avatar_url)
            embed.add_field(name="Rang " + str(counter), value=userRang, inline=False)
            await ctx.send(embed=embed)
            sended = True
            break
        counter += 1
    if not sended:
        await ctx.send("Kein Profil gefunden")
    await ctx.message.delete()


@bot.command()
async def rank(ctx, arg=None):
    await rang(ctx)


@bot.command()
async def wanted(ctx, user: discord.Member = None):
    if user == None:
        user = ctx.author
    wantedP = Image.open('wanted.jpg')
    wantedP = wantedP.convert('RGB')

    asset = user.avatar_url_as(size=128)
    with open('users.json', 'r') as f:
        users = json.load(f)
        xp = str(users[f'{user}']['experience'])
    tempxp = int(xp)
    aktLvl = int(tempxp ** (1 / 4))
    bounty = (aktLvl + 10) ** 2
    levelliste = [0, 0, 16, 81, 256, 625, 1296, 2401, 4096, 6561, 10000, 14641, 20736, 28561, 38416, 50625, 65536,
                  83521, 104976, 130321,
                  160000, 194481, 234256, 279841, 331776, 390625, 456976, 531441, 614656, 707281, 810000, 923521]
    ben_xp = levelliste[aktLvl + 1]
    data = BytesIO(await asset.read())
    pfp = Image.open(data)
    pfp = pfp.resize((382, 261))
    wantedP.paste(pfp, (60, 155))
    draw = ImageDraw.Draw(wantedP)
    fontW = ImageFont.truetype("Allan-Bold.ttf", 34)
    fontG = ImageFont.truetype("Allan-Bold.ttf", 42)
    draw.text(xy=(110, 490), text="Level: " + str(aktLvl) + "   XP: " + str(xp) + "/" + str(ben_xp),
              fill=(56, 36, 5, 0), font=fontW)
    draw.text(xy=(110, 550), text="BOUNTY: " + str(bounty), fill=(56, 36, 5, 0), font=fontG)
    color = (56, 36, 5)
    wantedP.save("profile.jpg")
    await ctx.send(file=discord.File("profile.jpg"))
    await ctx.message.delete()


def isChannel(idS):
    channelList = [785915511526785054, 701735405002817540, 701736801995456572, 772881952101105715,
                   701745690266566757, 772798084723900446, 772797937355980831, 772793727198167082, 771681324922044433,
                   772797804720029707]
    semester4 = [827623070483218463, 827623855425978399, 827626157150568448, 827626666611703869, 827627239259635752,
                 827627456650412102, 827630159506702376, 827630306046902282, 827630487350149180, 827630754624045096,
                 827631202625257493, 828214508203671602]
    channelList.append(semester4)

    if idS in channelList:
        return True
    else:
        return False


######################### Client Events #########################
@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    #if message.author.bot == False and isChannel(message.channel.id):
    if message.author.bot == False:
        with open('users.json', 'r') as f:
            users = json.load(f)
        exp = calcXP(message.content)
        await update_data(users, message.author)
        await add_experience(users, message.author, exp)
        await level_up(users, message.author, message.channel)
        with open('users.json', 'w') as f:
            json.dump(users, f)

    # if 'hilfe' in message.content.lower():
    # await message.channel.send("schon probiert neuzustarten?")
    '''
    if benachrichtigungA == "Benachrichtigungen aktivieren":
        if 'manuel' in message.content.lower():
            await message.channel.send('<@234396214555049994> wird sich gleich darum kümmern')
        if 'sören' in message.content.lower():
            await message.channel.send('<@618038763364417556> wo bleibst du? Wir wollen Bembel', tts=True)
        if 'melvin' in message.content.lower():
            await message.channel.send('<@330272807386873856> du sollst hier nachschauen!', tts=True)
        if 'doma' in message.content.lower():
            await message.channel.send('<@154337444844666880> schau mal hier!', tts=True)
        if 'saskia' in message.content.lower():
            await message.channel.send('Hallo <@694137575723499571>, du wurdest erwähnt.', tts=True)
        if 'yana' in message.content.lower():
            await message.channel.send('<@625770274318581801> nur damit du weißt!', tts=True)
        if 'lukas' in message.content.lower():
            await message.channel.send('Schon lange nicht mehr gesehen <@345950571288592384>, schau mal hier.', tts=True)
        if 'jan' in message.content.lower():
            await message.channel.send('Wo bist du <@122432121921863682>?', tts=True)
        if 'marco' in message.content.lower():
            await message.channel.send('Hey <@258338600997879812>, schau mal hier', tts=True)
    '''

    # if message.content.lower().startswith('test'):
    # await message.channel.send("Funktioniert auf dem Server hier :)")

    await bot.process_commands(message)


bot.run(authBot)
