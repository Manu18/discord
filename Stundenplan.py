#!/usr/bin/python3
import requests
from bs4 import BeautifulSoup
from google.cloud import firestore
import os

################## Basic Inits ################
fileHandle = open('auth.txt', "r")
lines = fileHandle.readlines()
fileHandle.close()
authBot = lines[0].rstrip("\n")
project_id = 'able-reef-297917'
credential_path = 'firestoreAuth.json'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path

########## DB #####################
db = firestore.Client()

########## HTML Parser ###############
headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Max-Age': '3600',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }
url = "https://eva2.inf.h-brs.de/stundenplan/anzeigen/?weeks=39%3B40%3B41%3B42%3B43%3B44%3B45%3B46%3B47%3B48%3B49%3B50%3B51%3B54%3B55&days=1-7&mode=table&identifier_semester=%23SPLUS4E1741&show_semester=&identifier_dozent=&term=e4e488484864f3046d4b77c253f3d3a6"
req = requests.get(url, headers)
soup = BeautifulSoup(req.content, 'html.parser')
html = soup.prettify()
table = soup.find("table")

kursliste = []
wochentag = "Mo"

for row in table.find_all('tr'):
    veranstaltung = "".join([x.text for x in row.find_all('td', class_='liste-veranstaltung', text=True)])
    if not veranstaltung:
        continue
    lwoche = "".join([x.text for x in row.find_all('td', class_='liste-wochentag', text=True)])
    if lwoche:
        wochentag = lwoche
    gruppe = ""
    lgruppe = veranstaltung[-8:-4]
    if lgruppe[0:3] == "Gr.":
        gruppe = lgruppe[3]

    vorlesung = {
        "start": "".join([x.text for x in row.find_all('td', class_='liste-startzeit', text=True)]),
        "end": "".join([x.text for x in row.find_all('td', class_='liste-endzeit', text=True)]),
        "raum": "".join([x.text for x in row.find_all('td', class_='liste-raum', text=True)]),
        "fach": "".join([x.text for x in row.find_all('td', class_='liste-veranstaltung', text=True)])[0:-4],
        "zeitraum": "".join([x.text for x in row.find_all('td', class_='liste-beginn', text=True)]),
        "prof": "".join([x.text for x in row.find_all('td', class_='liste-wer', text=True)]),
        "typ": "".join([x.text for x in row.find_all('td', class_='liste-veranstaltung', text=True)])[-2:-1],
        "semester": 5,
        "tag": wochentag,
        "link": "https://lea.hochschule-bonn-rhein-sieg.de/",
        "gruppe": gruppe,
        "spezialisierung": ""
    }
    kursliste.append(vorlesung)

kurseDB = db.collection(u'StundenplanWS2021')
for kurse in kursliste:
    titel = kurse['fach'].replace("/","|") + " (" + kurse['typ'] + ") (" + kurse['tag'] + ")"
    print(titel)
    kurseDB.document(titel).set(kurse)
'''
kurseDB.document("Computergrafik (P) (Di)").set(
    {
'end': "13:45",
'fach': "Computergrafik",
'gruppe': "",
'link': "https://lea.hochschule-bonn-rhein-sieg.de/",
'prof': "Bachmann",
'raum': "",
'semester': 4,
'spezialisierung': "Visual Computing",
'start': "13:00",
'tag': "Di",
'typ': "P",
'zeitraum': "06.04.2021-06.07.2021 (KW 14-27)"
}
)
'''