#!/usr/bin/env python
import logging
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)
from datetime import datetime, timedelta
import schedule

f = open('auth.txt')
lines = f.readlines()
auth = str(lines[1])

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def start(update: Update, context: CallbackContext):
    update.message.reply_text('Bot ist nun online')


def help_command(update: Update, context: CallbackContext):
    update.message.reply_text('Hier ist Hilfe!')


def echo(update: Update, context: CallbackContext):
    update.message.reply_text(update.message.text)

def textActions(update: Update, context: CallbackContext):
    atw = antworten(update.message.text)
    print(atw)
    update.message.reply_text(atw)

def antworten(text):
    if 'laufen' in text:
        updateLaufen()
        return 'Will jemand mit Laufen gehen @mamf18'
    else:
        return 'unknown'


def updateLaufen():
    date = str(datetime.now())
    s = 'Letzters mal Laufen: ' + date + '\n'
    with open("telegram.txt", mode='w') as file:
        file.write(s)

def zuletztGelaufen():
    fileHandle = open ( 'telegram.txt',"r" )
    lineList = fileHandle.readlines()
    fileHandle.close()
    s = str(lineList[-1])
    print(s)
    ll = datetime.strptime(s[21:-17],"%Y-%m-%d")
    return ll

def lastLaufen(bot, update, job_queue):
    ll = zuletztGelaufen()
    delta = datetime.now() - timedelta(days=3)
    #job = job_queue.run_repeating(sayhi, 5, context=update)
    if delta > ll:
        s = "Laufen gehen!!!"
        print(s)
        bot.send_message(channelID, text=s)

def main():
    updater = Updater(auth, use_context=True)
    dispatcher = updater.dispatcher
    bot = Bot(token=auth)
    chatID = 803057354
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("echo", echo))
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, textActions))
    dispatcher.add_handler(MessageHandler(Filters.text, lastLaufen, pass_job_queue=True))
    
    
    #schedule.every().any_day_of_the_week.at(MM:SS).do(lastLaufen())
    #schedule.every(1).minutes.do(lastLaufen(bot, chatID))

    updater.start_polling()
    updater.idle()



if __name__ == '__main__':
    main()